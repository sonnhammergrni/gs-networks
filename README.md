# GeneSPIDER networks #

These GeneSPIDER networks where created with the [GeneSPIDER toolbox](https://bitbucket.org/sonnhammergrni/genespider) and are stored in the structure provided by the toolbox [datastruct](https://bitbucket.org/sonnhammergrni/datastruct) in `.json` format.

### What is this repository for? ###

* GeneSPIDER networks can be used for creating datasets for benchmarking network inference algorithms, or together with the pre-made [datasets](https://bitbucket.org/sonnhammergrni/gs-datasets), used for benchmarking directly.

### How do I get set up? ###

* How to get it:

    Fetch this repository with the command

        git clone git@bitbucket.org:sonnhammergrni/gs-networks.git ~/src/gs_networks

    or download from [here](https://bitbucket.org/sonnhammergrni/gs-networks/downloads).

* How do I use it with the GeneSPIDER toolbox [datastruct](https://bitbucket.org/sonnhammergrni/datastruct)?:

    1. If the repository is downloaded simply load the networks in MATLAB with the command

            Net = datastruct.Network.load('path/to/file.json')

         where `path/to/file.json` should be exchanged for an actual path to a network file.

    2. Any networks can be fetched directly from the on-line repository with the command

            Net = datastruct.Network.fetch('name-of-network.json')

         where `name-of-network.json` is the actual name as found in this repository. A complete URL to the network can also be provided. The `master` version of the file is fetched, meaning that if anything changes in the file, a different network will be fetched and loaded. If a specific version is desired than provide the full path to the raw file including the hash to that commit instead.

* How do I get datasets in to python?

    Currently there are no supported GeneSPIDER datastructure types in python, however it is still easy to load the data in to python using the `pandas` library like this

        import pandas
        Net = pandas.read_json(<URL>).obj_data

    where <URL> is a path to a file, local or remote, as found in this repository.  
    The elements of `Net` can now be accessed by indexing, e.g.

        Net.['A'] 

    will return the network matrix as a list.

* Dependencies

    No strict dependencies, however the [GeneSPIDER toolbox](https://bitbucket.org/sonnhammergrni/genespider) is recommended.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

[![Creative Commons
License](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

These `GeneSPIDER networks` are licensed under a [Creative Commons Attribution
4.0 International License](http://creativecommons.org/licenses/by/4.0/).
